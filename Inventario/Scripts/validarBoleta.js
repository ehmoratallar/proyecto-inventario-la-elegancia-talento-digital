﻿$(document).ready(function () {
    $("#frmBoleta").validate({
        rules: {
            ctl00$cphPrincipal$txtFecha: {
                required: true
            },
            ctl00$cphPrincipal$txtUsuario: {
                required: true
            },
            ctl00$cphPrincipal$txtDescripcion: {
                required: true,
                minlength: 5
            }

        },
        messages: {
            ctl00$cphPrincipal$txtFecha: {
                required: "Este campo es requerido"
            },
            ctl00$cphPrincipal$txtUsuario: {
                required: "Este campo es requerido"
            },
            ctl00$cphPrincipal$txtDescripcion: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            }

        }
    });
});