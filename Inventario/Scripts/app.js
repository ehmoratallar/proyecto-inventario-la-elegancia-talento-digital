function $_GET(param) {
    /* Obtener la url completa */
    var url = document.URL;
    /* Buscar a partir del signo de interrogación ? */
    url = String(url.match(/\?+.+/));
    /* limpiar la cadena quitándole el signo ? */
    url = url.replace("?", "");
    /* Crear un array con parametro=valor */
    url = url.split("&");

    /* 
    Recorrer el array url
    obtener el valor y dividirlo en dos partes a través del signo = 
    0 = parametro
    1 = valor
    Si el parámetro existe devolver su valor
    */
    x = 0;
    while (x < url.length) {
        p = url[x].split("=");
        if (p[0] == param) {
            return decodeURIComponent(p[1]);
        }
        x++;
    }
}


var app = angular.module("inventario", []);

app.controller("productController", function ($scope, $http){

	$scope.message = "Hey";
	$scope.products = [{name :'producto 1'}, {name :'producto 2'}];
	$scope.products = [];
	
	$scope.BoletaDetalleProductos = [];
	
	$scope.showSelectPanel = true;
	
	$scope.baseURL = "http://localhost:50452/api/";
	
	//Paginacion
	$scope.currentPage = 0;
    $scope.pageSize = 2;
	$scope.numberOfPages = function (){
        return Math.ceil($scope.products.length/$scope.pageSize);               
    }
	
	
	$scope.getProductos = function(query){
		$scope.products = [];
		
		if(!$scope.productFilter){
			query = "undefined";
		}
		
		$http.get($scope.baseURL + "Productos/BuscarProducto/" + query)
		.then(function(response) {
			//console.log("Get Products Response");
			//console.log(response);
			$scope.products = response.data;
			$scope.currentPage = 0;
		});
	};
	
	$scope.getBoletaTipoMovimientos = function(){
		$scope.BoletaTipoMovimientos = [];

		$http.get($scope.baseURL + "BoletaDetalle/ObtenerBoletaTipoMovimiento/")
		.then(function(response) {
			//console.log("Get Tipo Boletas");
			//console.log(response);
			$scope.BoletaTipoMovimientos = response.data;
		});
	};
	
	$scope.seleccionarProducto = function(producto){
		$scope.showSelectPanel = false;
		$scope.productoSeleccionado = producto;
		$scope.CodBoleta = $_GET("CodBoleta");
	};
	
	$scope.agregarNuevoDetalle = function(){
		//console.log("Debug");
		//alert("Submit");
		
		//Estamos utilizando las variables
		//$scope.productoSeleccionado
		//$scope.BoletaNuevoDetalle
		
		//Validar si no existe en la lista $scope.BoletaDetalleProductos; tomando en cuenta el tipo de movimiento
		var result = $.grep($scope.BoletaDetalleProductos, function(e) { 
							return  (e.iCodProducto == $scope.productoSeleccionado.iCodProducto && e.iCodTipoMovimiento == $scope.BoletaNuevoDetalle.TipoMovimiento.iCodTipoMovimiento); 
						});
		//console.log("Validando si el producto y tipo de movimiento existen");
		//console.log(result);
		
		if(result.length == 0){
			//No está en la lista, debemos insertarlo
			
			var BoletaDetalle = new Object();
			BoletaDetalle.iCodBoleta = $scope.CodBoleta;
			BoletaDetalle.iCodProducto  = $scope.productoSeleccionado.iCodProducto;
			BoletaDetalle.iCodTipoMovimiento = $scope.BoletaNuevoDetalle.TipoMovimiento.iCodTipoMovimiento;
			BoletaDetalle.sNombre = $scope.productoSeleccionado.sNombre;
			BoletaDetalle.iCantidad = $scope.BoletaNuevoDetalle.Cantidad;
			BoletaDetalle.sJustificacion = $scope.BoletaNuevoDetalle.Justificacion;
			BoletaDetalle.producto = $scope.productoSeleccionado;
			BoletaDetalle.TipoMovimiento = $scope.BoletaNuevoDetalle.TipoMovimiento;
			BoletaDetalle.ProductoDaniado = $scope.BoletaNuevoDetalle.ProductoDaniado;
			
			
			$scope.BoletaDetalleProductos.push(BoletaDetalle);
			
			
		}
		else{
			alert("El producto ya se encuentra en la boleta con este tipo de movimiento.");
			
		}
		
		$scope.showSelectPanel = true;
		$scope.productoSeleccionado = null;
		$scope.BoletaNuevoDetalle = null;
		
		//console.log("$scope.BoletaDetalleProductos");
		//console.log($scope.BoletaDetalleProductos);
		
	};
	
	
	$scope.guardarBoleta = function(){
		if(confirm("Desea guardar la boleta?")){
		    var config = {
		        headers: {
		            'Content-Type': 'application/json'
		        }
		    }

		    $http.post($scope.baseURL + "BoletaDetalle/Post", { data: $scope.BoletaDetalleProductos }, config)
		   .then(
			   function (response) {
			       // success callback
			       console.log("Success");
			       console.log(response);
			   },
			   function (response) {
			       // failure callback
			       console.log("Fail");
			       console.log(response);
			   }
			);
		}
	};
	
	
	$scope.remover = function(BoletaDetalle){
		$scope.BoletaDetalleProductos = jQuery.grep($scope.BoletaDetalleProductos , function (value) {
				return value != BoletaDetalle;
		});
	};
	
	//Inicializar productos
	$scope.getProductos("undefined");
	$scope.getBoletaTipoMovimientos();
	
});


app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});


