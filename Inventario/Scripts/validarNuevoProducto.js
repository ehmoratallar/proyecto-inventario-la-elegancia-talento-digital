﻿$(document).ready(function() {
    $("#frmNuevoProducto").validate({
        rules: {
            ctl00$cphPrincipal$txtSKU: {
                required: true,
                minlength: 5
            },
            ctl00$cphPrincipal$txtNombre:{
                required: true,
                minlength: 5
            },
            ctl00$cphPrincipal$txtDescripcion: {
                minlength: 10
            },
            ctl00$cphPrincipal$txtPrecioVenta: {
                required:true,
                number: true
            },
            ctl00$cphPrincipal$txtCosto: {
                required: true,
                number: true
            }
            
        },
        messages: {
            ctl00$cphPrincipal$txtSKU: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtNombre: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtDescripcion: {
                minlength: "Debe ingresar 10 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtPrecioVenta: {
                required: "Este campo es requerido",
                number: "Debe ingresar un número decimal"
            },
            ctl00$cphPrincipal$txtCosto:{
                required: "Este campo es requerido",
                number: "Debe ingresar un número decimal"
            }
            
        }
    });



    $("#frmEditarProducto").validate({
        rules: {
            ctl00$cphPrincipal$txtSKU: {
                required: true,
                minlength: 5
            },
            ctl00$cphPrincipal$txtNombre: {
                required: true,
                minlength: 5
            },
            ctl00$cphPrincipal$txtDescripcion: {
                minlength: 10
            },
            ctl00$cphPrincipal$txtPrecioVenta: {
                required: true,
                number: true
            },
            ctl00$cphPrincipal$txtCosto: {
                required: true,
                number: true
            }

        },
        messages: {
            ctl00$cphPrincipal$txtSKU: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtNombre: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtDescripcion: {
                minlength: "Debe ingresar 10 caracteres mínimo"
            },
            ctl00$cphPrincipal$txtPrecioVenta: {
                required: "Este campo es requerido",
                number: "Debe ingresar un número decimal"
            },
            ctl00$cphPrincipal$txtCosto: {
                required: "Este campo es requerido",
                number: "Debe ingresar un número decimal"
            }

        }
    });

});
