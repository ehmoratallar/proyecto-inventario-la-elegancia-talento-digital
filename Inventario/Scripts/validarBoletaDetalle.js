$(document).ready(function () {
    $("#frmBoleta").validate({
        rules: {
            tipoMovimiento: {
                required: true
            },
            cantidad: {
                required: true
            },
            justificacion: {
                required: true,
                minlength: 5
            }

        },
        messages: {
            tipoMovimiento: {
                required: "Este campo es requerido"
            },
            cantidad: {
                required: "Este campo es requerido"
            },
            justificacion: {
                required: "Este campo es requerido",
                minlength: "Debe ingresar 5 caracteres mínimo"
            }

        }
    });
});