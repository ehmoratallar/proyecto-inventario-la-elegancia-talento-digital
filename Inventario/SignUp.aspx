﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Inventario.SignUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <!-- Primera fila Grid -->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="block-center">
                <h1>Registrar nuevo Usuario</h1>
            </div>
        </div>
    </div>
    <!-- Primera fila Grid -->
    <!-- Segunda Fila -->
    <div class="row">
        <!-- Primera Columna -->
        <div class="col-xs-1 col-md-1 col-lg-1">
        </div>
        <!-- Segunda Columna -->
        <div class="col-xs-11 col-md-11 col-lg-11">
            <form id="frmSignUp" runat="server">
                <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" ContinueDestinationPageUrl="~/App/Main.aspx" >
                    <WizardSteps>
                        <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                        </asp:CreateUserWizardStep>
                        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                        </asp:CompleteWizardStep>
                    </WizardSteps>
                </asp:CreateUserWizard>
            </form>
        </div>
    </div>
    <!-- Segunda Fila -->
</asp:Content>
