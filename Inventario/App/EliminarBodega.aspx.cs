﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class EliminarBodega : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        private Bodega bodega;

        protected void Page_Load(object sender, EventArgs e)
        {

            string sReturnURL = "/App/GestorBodegas.aspx";

            if (Request.QueryString["iBodega"] != null)
            {
                Boolean isNum = false;
                int iCodBodega;
                isNum = int.TryParse(Request.QueryString["iBodega"], out iCodBodega);

                if (!isNum)
                {

                    sMensajeError = "Bodega inválida";
                    sReturnURL += "?sMensajeError=" + sMensajeError;

                }
                else
                {

                    this.bodega = Bodega.get(iCodBodega);

                    if (bodega == null)
                    {

                        sMensajeError = "Bodega inválida";
                        sReturnURL += "?sMensajeError=" + sMensajeError;
                    }
                    else
                    {
                        try
                        {
                            bodega.eliminar();
                            sMensajeExito = "Se ha eliminado la bodega.";
                            sReturnURL += "?sMensajeExito=" + sMensajeExito;
                        }
                        catch (Exception ex) {
                            sReturnURL += "?sMensajeError=" + ex.Message;
                        }
                        
                        
                    }
                }

            }

            Response.Redirect(sReturnURL);

        }
    }
}