﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class NuevoProducto : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        protected void btnNuevoProducto_Click(object sender, EventArgs e)
        {
            Producto p = new Producto();


            p.sSKU = txtSKU.Text;
            p.sNombre = txtNombre.Text;
            p.sDescripcion = txtDescripcion.Text;
            p.sFabricante = txtFabricante.Text;
            p.dPrecio = decimal.Parse(txtPrecioVenta.Text);
            p.dCosto = decimal.Parse(txtCosto.Text);



            int newCodProducto = p.guardar();
            Boolean success = newCodProducto > 0;
            if (success)
            {
             
                bMostrarMensajeExito = true;
                sMensajeExito = "Nuevo producto creado correctamente. ";
                bMostrarMensajeExito = true;

                txtSKU.Text = "";
                txtNombre.Text = "";
                txtDescripcion.Text = "";
                txtFabricante.Text = "";
                txtPrecioVenta.Text = "";
                txtCosto.Text = "";

            }
            else
            {
                bMostrarMensajeError = true;
                sMensajeError = "No se ha podido guardar el producto. ";
                bMostrarMensajeError = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}