﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NuevaBoleta.aspx.cs" Inherits="Inventario.App.NuevaBoleta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery.validate.js"></script>
    <script src="../Scripts/validarBoleta.js"></script>
    <style>
        #frmBoleta label.error {
            margin-left: 10px;
            width: auto;
            display: inline;
            color: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError)
            { %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito)
            { %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div id="title">
                    <i class="fa fa-plus fa-5x"></i>
                    <h1>Nueva Boleta</h1>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>



    <%-- Comienza formulario --%>
    <form id="frmBoleta" name="frmBoleta" action="NuevaBoleta.aspx" method="post" runat="server">
        <%-- Row --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Información de boleta</h3>
                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <label>Fecha: </label>
                            <asp:TextBox ID="txtFecha" name="txtSKU" runat="server" CssClass="form-control" MaxLength="50" ReadOnly="True"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Usuario: </label>
                            <asp:TextBox ID="txtUsuario" name="txtNombre" runat="server" CssClass="form-control" MaxLength="150" ReadOnly="True"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Bodega: </label>
                            <div class="input-group">

                                <asp:TextBox ID="txtBodega" name="txtNombre" runat="server" CssClass="form-control" MaxLength="150" ReadOnly="True"></asp:TextBox>
                                <span class="input-group-btn">
                                    <p><a href="Main.aspx?resetLocation=true" class="btn btn-default" role="button">Cambiar</a></p>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Descripción: </label>
                            <asp:TextBox ID="txtDescripcion" name="txtNombre" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <%-- /Row --%>




        <asp:Button ID="btnNuevaBoleta" runat="server" Text="Crear Boleta" CssClass="btn btn-primary" OnClick="btnNuevaBoleta_Click" />

        <%-- Finaliza Formulario --%>
    </form>

</asp:Content>
