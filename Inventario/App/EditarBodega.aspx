﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EditarBodega.aspx.cs" Inherits="Inventario.App.EditarBodega" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <div class="row">
        <a href="/App/GestorBodegas.aspx">
            <button type="button" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button>
        </a>
    </div>

        <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError) { %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito) { %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div id="title">
                    <i class="fa fa-pencil fa-5x"></i>
                    <h1>Editar Bodega</h1>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Editar Información Bodega</h3>
                </div>
                <div class="panel-body">

                    <form name="frmNuevaBodega" id="frmEditarBodega" action="EditarBodega.aspx" method="post" runat="server">
                        <div class="form-group">
                            <label>Nombre bodega: </label>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Direccion bodega: </label>
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                        </div>
                        <asp:HiddenField ID="txtCodBodega" runat="server" />

                        <asp:Button ID="btnEditarBodega" runat="server" Text="Editar Bodega" CssClass="btn btn-primary" OnClick="btnEditarBodega_Click"  />

                    </form>

                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>

</asp:Content>
