﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NuevoProducto.aspx.cs" Inherits="Inventario.App.NuevoProducto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery.validate.js"></script>
    <script src="../Scripts/validarNuevoProducto.js"></script>
    <style>

        #frmNuevoProducto label.error {
		margin-left: 10px;
		width: auto;
		display: inline;
        color:red;
	}

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError)
            { %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito)
            { %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div id="title">
                    <i class="fa fa-plus fa-5x"></i>
                    <h1>Nuevo Producto</h1>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>

    <%-- Comienza formulario --%>
    <form  id="frmNuevoProducto" name="frmNuevoProducto" action="NuevoProducto.aspx" method="post" runat="server">
        <%-- Row --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Información de producto</h3>
                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <label>SKU: </label>
                            <asp:textbox id="txtSKU" name="txtSKU" runat="server" cssclass="form-control" maxlength="50"></asp:textbox>
                        </div>
                        <div class="form-group">
                            <label>Nombre: </label>
                            <asp:textbox id="txtNombre" name="txtNombre" runat="server" cssclass="form-control" maxlength="150"></asp:textbox>
                        </div>
                        <div class="form-group">
                            <label>Descripción: </label>
                            <asp:textbox id="txtDescripcion" name="txtNombre" runat="server" cssclass="form-control" maxlength="150"></asp:textbox>
                        </div>
                        <div class="form-group">
                            <label>Fabricante: </label>
                            <asp:textbox id="txtFabricante" name="txtNombre" runat="server" cssclass="form-control" maxlength="150"></asp:textbox>
                        </div>
                        



                    </div>
                </div>
            </div>
        </div>
        <%-- /Row --%>


        <%-- Row --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Información de precio</h3>
                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <label>Precio de venta: </label>
                            <asp:textbox id="txtPrecioVenta" name="txtPrecioVenta" runat="server" cssclass="form-control" maxlength="50"></asp:textbox>
                        </div>
                        <div class="form-group">
                            <label>Costo: </label>
                            <asp:textbox id="txtCosto" name="txtCosto" runat="server" cssclass="form-control" maxlength="150"></asp:textbox>
                        </div>

                        



                    </div>
                </div>
            </div>
        </div>
        <%-- /Row --%>

        <asp:button id="btnNuevoProducto" runat="server" text="Crear Producto" cssclass="btn btn-primary" OnClick="btnNuevoProducto_Click" />

        <%-- Finaliza Formulario --%>
    </form>

</asp:Content>
