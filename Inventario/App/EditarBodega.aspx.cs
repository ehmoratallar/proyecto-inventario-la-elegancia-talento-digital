﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class EditarBodega : System.Web.UI.Page
    {
        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        private Bodega bodega;


        protected void btnEditarBodega_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != null && txtNombre.Text != "")
            {
                if (txtDireccion.Text != null && txtDireccion.Text != "")
                {
                    //Bodega b = new Bodega(bodega.iCodBodega,txtNombre.Text, txtDireccion.Text);
                    bodega = new Bodega();
                    bodega.iCodBodega = Convert.ToInt32(txtCodBodega.Value);
                    bodega.sNombre = txtNombre.Text;
                    bodega.sDireccion = txtDireccion.Text;

                    Boolean success = bodega.editar(); 
                    if (success)
                    {
                       
                        bMostrarMensajeExito = true;
                        sMensajeExito = "Bodega editada correctamente";
                        bMostrarMensajeExito = true;
                        txtDireccion.Text = "";
                        txtNombre.Text = "";
                    }
                    else
                    {
                        bMostrarMensajeError = true;
                        sMensajeError = "No se ha podido modificar la bodega. ";
                        bMostrarMensajeError = true;
                    }

                }
                else
                {
                    bMostrarMensajeError = true;
                    sMensajeError = "Debe de completar todos los campos con valores válidos";
                    bMostrarMensajeError = true;
                }
            }
            else
            {
                bMostrarMensajeError = true;
                sMensajeError = "Debe de completar todos los campos con valores válidos";
                bMostrarMensajeError = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["iBodega"] != null)
            {
                Boolean isNum = false;
                int iCodBodega;
                isNum = int.TryParse(Request.QueryString["iBodega"], out iCodBodega);

                if (!isNum)
                {
                   
                    sMensajeError = "Bodega inválida";
                    bMostrarMensajeError = true;

                }
                else {
                    
                    this.bodega = Bodega.get(iCodBodega);

                    if (bodega == null)
                    {
                       
                        sMensajeError = "Bodega inválida";
                        bMostrarMensajeError = true;
                    }
                    else {
                       
                        txtNombre.Text = bodega.sNombre;
                        txtDireccion.Text = bodega.sDireccion;
                        txtCodBodega.Value = Convert.ToString(bodega.iCodBodega);
                    }
                }

            }



        }
    }
}