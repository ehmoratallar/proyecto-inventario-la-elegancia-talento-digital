﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="GestorBodegas.aspx.cs" Inherits="Inventario.App.GestorBodegas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError)
            { %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito)
            { %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div id="title">
                    <i class="fa fa-cogs fa-5x"></i>
                    <h1>Gestor Bodegas</h1>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Bodegas - Mostrando <%= (iResultadosPorPagina>iTotalResultados)?iTotalResultados.ToString():iResultadosPorPagina.ToString() %> de <%= iTotalResultados.ToString() %></h3>
                </div>
                <div class="panel-body">

                    <%-- Crear la tabla --%>

                    <%-- Si no hay datos, mostrar mensaje --%>
                    <% if (lstBodegas != null && lstBodegas.Count < 1)
                        { %>
                    No hay bodegas disponibles

                    <%}%>

                    <%else
                        {%>
                    <%--//Definir tabla--%>
                    <div class="table-responsive">

                        <table class="table table-hover">
                            <%--//Crear headers--%>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Direccion</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    //Llenar Tabla
                                    Inventario.App.Bodega b;
                                    if (lstBodegas != null)
                                    {
                                        for (int i = 0; i < lstBodegas.Count; i++)
                                        {
                                            b = (Inventario.App.Bodega)lstBodegas[i];

                                %>
                                <tr>
                                    <td><%= b.sNombre %></td>
                                    <td><%= b.sDireccion %></td>
                                    <td>
                                        <%-- Botones acciones --%>
                                        <a href="/App/EditarBodega.aspx?iBodega=<%=b.iCodBodega %>" > <button type="button" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button></a>
                                        <a href="/App/EliminarBodega.aspx?iBodega=<%=b.iCodBodega %>" > <button type="button" class="btn btn-danger" onclick="return confirm('Desea eliminar esta bodega？Esta operación no puede deshacerse')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</button></a>

                                    </td>
                                </tr>
                                <%

                                        }

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>

                    <%-- Paginacion --%>
                    <%
                        //Paginacion
                        Response.Write(generarPaginacion("/App/GestorBodegas.aspx"));
                    %>


                    <%} %>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>
</asp:Content>
