﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Inventario.Authenticated.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError)
            { %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito)
            { %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>


    <div class="row">
        <div class="col-sm-12">

            <div class="jumbotron">
                <h1>Bienvenido <%= Membership.GetUser().UserName %></h1>
            </div>
        </div>

    </div>

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Area Información</h3>
                </div>
                <div class="panel-body">
                    <%--<asp:Label ID="InfoLabel" runat="server" Text="Label"></asp:Label>--%>

                    

                    <%-- Generar thumbnails --%>

                    <%

                        if (!CookieIsSet)
                        {

                    %>

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Seleccione su ubicación actual:</h3>

                        </div>

                    </div>

                    <div class="row">
                        <%Inventario.App.Bodega bodega = new Inventario.App.Bodega(); %>
                        <% for (int i = 0; i < lstBodegas.Count; i++)
                            {
                                bodega = (Inventario.App.Bodega)lstBodegas[i];
                        %>

                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">

                                <div class="caption">
                                    <h3><%= bodega.sNombre %></h3>
                                    <p><%= bodega.sDireccion %></p>
                                    <p><a href="Main.aspx?setLocation=<%= bodega.iCodBodega %>" class="btn btn-default" role="button">Seleccionar</a></p>
                                </div>
                            </div>
                        </div>

                        <%} %>
                    </div>


                    <%}

                        else
                        {
                            Inventario.App.Bodega bodegaActual = Inventario.App.Bodega.get(iCodBodegaActual);
                    %>

                    <div class="row">
                        <div class="col-sm-12">
                            <p><h3>Ubicación actual: <%= bodegaActual.sNombre %></h3></p>
                            <p><%= bodegaActual.sDireccion %></p>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <p><a href="Main.aspx?resetLocation=true" class="btn btn-default" role="button">Seleccionar otra ubicación</a></p>
                        </div>
                    </div>

                    <%} %>


                    <%-- / Generar thumbnails --%>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

</asp:Content>
