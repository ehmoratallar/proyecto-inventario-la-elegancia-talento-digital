﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class EliminarProducto : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        private Producto producto;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturnURL = "/App/GestorProductos.aspx";

            if (Request.QueryString["iCodProducto"] != null)
            {
                Boolean isNum = false;
                int iCodProducto;
                isNum = int.TryParse(Request.QueryString["iCodProducto"], out iCodProducto);

                if (!isNum)
                {

                    sMensajeError = "Producto inválido";
                    sReturnURL += "?sMensajeError=" + sMensajeError;

                }
                else
                {

                    this.producto = Producto.get(iCodProducto);

                    if (producto == null)
                    {

                        sMensajeError = "Producto inválido";
                        sReturnURL += "?sMensajeError=" + sMensajeError;
                    }
                    else
                    {
                        try
                        {
                            producto.eliminar();
                            sMensajeExito = "Se ha eliminado el producto.";
                            sReturnURL += "?sMensajeExito=" + sMensajeExito;
                        }
                        catch (Exception ex)
                        {
                            //sReturnURL += "?sMensajeError=" + ex.Message.ToString();
                            sReturnURL += "?sMensajeError=Ha ocurrido un error en la base de datos.";
                        }


                    }
                }

            }

            Response.Redirect(sReturnURL);

        }
    }
}