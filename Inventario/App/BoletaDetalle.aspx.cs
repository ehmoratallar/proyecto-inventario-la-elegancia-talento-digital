﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class BoletaDetalle : System.Web.UI.Page
    {
        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        public static HttpCookie Cookie = new HttpCookie("CodBodegaActual");
        public static int iCodBodegaActual;
        public Boolean CookieIsSet = false;

        public static Boleta boleta;
        public static int iCodBoleta;
        public static string sBusqueda = "";

        public static Producto productoSeleccionado;

        

        //Paginacion
        public static int iResultadosPorPagina;
        public static int iPagina;
        public static int iTotalResultados;
        public static int iPaginasDeContenido = 0;

        public static int iPaginasIzquierda = 2;
        public static int iPaginasDerecha = 2;

        public List<Producto> lstProductos;

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //if (txtBusqueda.Text != "")
            //{
            //Response.Redirect("BoletaDetalle.aspx?CodBoleta=" + iCodBoleta + "&sBusqueda=" + txtBusqueda.Text);
            //}
            //else {
            //    Response.Redirect("BoletaDetalle.aspx?CodBoleta=" + iCodBoleta);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["sBusqueda"] != null) {
               // txtBusqueda.Text = Request.QueryString["sBusqueda"];

            }

            //Fetch the Cookie using its Key.
            Cookie = Request.Cookies["CodBodegaActual"];
            if (Cookie != null)
            {
                CookieIsSet = true;
                iCodBodegaActual = Convert.ToInt32(Cookie.Value.Split('=')[1]);
            }


            //Validamos si la boleta es válida
            if (Request.QueryString["CodBoleta"] != null)
            {

                Boolean isNum = int.TryParse(Request.QueryString["CodBoleta"], out iCodBoleta);
                if (!isNum)
                {
                    //El parámetro es incorrecto, redirigir y mostrar mensaje de error
                    Response.Redirect("/App/NuevaBoleta.aspx?sMensajeError=Parámetro inválido");
                }
                else
                {
                    //El parámetro es válido, validar si existe la boleta
                    boleta = Boleta.get(iCodBoleta);

                    if (boleta == null)
                    {
                        //Boleta inválida, redirect
                        Response.Redirect("/App/NuevaBoleta.aspx?sMensajeError=Boleta inválida");
                    }
                    else
                    {
                        //Parametros correctos

                        //Validar argumentos
                        if (Request.QueryString["resultadosPorPagina"] != null)
                        {

                            isNum = int.TryParse(Request.QueryString["resultadosPorPagina"], out iResultadosPorPagina);
                            if (!isNum)
                            {
                                iResultadosPorPagina = 3;
                            }
                        }
                        else
                        {
                            iResultadosPorPagina = 3;
                        }


                        if (Request.QueryString["pagina"] != null)
                        {

                            isNum = int.TryParse(Request.QueryString["pagina"], out iPagina);
                            if (!isNum)
                            {
                                iPagina = 0;
                            }
                        }
                        else
                        {
                            iPagina = 0;
                        }

                        if (Request.QueryString["sMensajeExito"] != null)
                        {
                            sMensajeExito = Request.QueryString["sMensajeExito"];
                            bMostrarMensajeExito = true;
                        }

                        if (Request.QueryString["sMensajeError"] != null)
                        {
                            sMensajeError = Request.QueryString["sMensajeError"];
                            bMostrarMensajeError = true;
                        }


                        //Llenar información de Bodegas
                       // lstProductos = Producto.getProductosPorBodega(iCodBodegaActual, txtBusqueda.Text, iResultadosPorPagina, iPagina);
                       // iTotalResultados = Producto.getTotalProductos();
                        //iPaginasDeContenido = iTotalResultados / iResultadosPorPagina;


                        // FIN Parametros correctos
                    }


                }
            }

        }



        public static string generarPaginacion(string urlBase)
        {
            string sResultado = "";
            StringBuilder sb = new StringBuilder();

            if (iPaginasDeContenido > 0)
            {
                sb.Append("<nav aria-label=\"...\">");
                sb.Append("<ul class=\"pagination\">");
                //Habilitar o deshabilitar Flecha anterior
                if (iPagina == 0)
                {
                    sb.Append("<li class=\"disabled\"><a href=\"\" aria-label=\"Anterior\"><span aria-hidden=\"true\">&laquo;</span></a></li>");
                }
                else
                {
                    sb.AppendFormat("<li><a href=\"{0}?resultadosPorPagina={1}&pagina={2}\" aria-label=\"Anterior\"><span aria-hidden=\"true\">&laquo;</span></a></li>", urlBase, iResultadosPorPagina, iPagina - 1);
                }
                //Dibujar paginas

                string sActive = "";
                for (int i = iPagina - iPaginasIzquierda; i <= iPagina + iPaginasDerecha; i++)
                {
                    sActive = "";
                    if (i == iPagina)
                    {
                        sActive = "class=\"active\"";
                    }

                    if (i >= 0 && i < iPaginasDeContenido)
                    {
                        sb.AppendFormat("<li {0}><a href=\"{1}?resultadosPorPagina={2}&pagina={3}\">{4}</a></li>", sActive, urlBase, iResultadosPorPagina, i, i + 1);
                    }
                }




                //Habilitar o deshabilitar Flecha siguiente
                if (iPagina == iPaginasDeContenido - 1)
                {
                    sb.Append("<li class=\"disabled\"><a href=\"\" aria-label=\"Siguiente\"><span aria-hidden=\"true\">&raquo;</span></a></li>");
                }
                else
                {
                    sb.AppendFormat("<li><a href=\"{0}?resultadosPorPagina={1}&pagina={2}\" aria-label=\"Siguiente\"><span aria-hidden=\"true\">&raquo;</span></a></li>", urlBase, iResultadosPorPagina, iPagina + 1);
                }

                sb.Append("</ul>");
                sb.Append("</nav>");
            }

            sResultado = sb.ToString();
            return sResultado;
        }

    }
}