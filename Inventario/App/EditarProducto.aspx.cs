﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class EditarProducto : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        public Producto producto;

        protected void btnNuevoProducto_Click(object sender, EventArgs e)
        {
            producto = new Producto();

            producto.iCodProducto = Convert.ToInt32(txtCodProducto.Value);
            producto.sSKU = txtSKU.Text;
            producto.sNombre = txtNombre.Text;
            producto.sDescripcion = txtDescripcion.Text;
            producto.sFabricante = txtFabricante.Text;
            producto.dPrecio = decimal.Parse(txtPrecioVenta.Text);
            producto.dCosto = decimal.Parse(txtCosto.Text);

            Boolean success = producto.editar();
            if (success)
            {

                bMostrarMensajeExito = true;
                sMensajeExito = "Producto editado correctamente.";
                bMostrarMensajeExito = true;

            }
            else
            {
                bMostrarMensajeError = true;
                sMensajeError = "No se ha podido modificar la información. ";
                bMostrarMensajeError = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["iCodProducto"] != null)
            {
                Boolean isNum = false;
                int iCodProducto;
                isNum = int.TryParse(Request.QueryString["iCodProducto"], out iCodProducto);

                if (!isNum)
                {

                    sMensajeError = "Producto inválido";
                    bMostrarMensajeError = true;

                }
                else
                {

                    this.producto = Producto.get(iCodProducto);

                    if (producto == null)
                    {

                        sMensajeError = "Producto inválido";
                        bMostrarMensajeError = true;
                    }
                    else
                    {

                        txtSKU.Text = producto.sSKU;
                        txtNombre.Text = producto.sNombre;
                        txtDescripcion.Text = producto.sDescripcion;
                        txtFabricante.Text = producto.sFabricante;
                        txtPrecioVenta.Text = Convert.ToString(producto.dPrecio);
                        txtCosto.Text = Convert.ToString(producto.dCosto);

                        txtCodProducto.Value = Convert.ToString(producto.iCodProducto);
                    }
                }

            }
        }
    }
}