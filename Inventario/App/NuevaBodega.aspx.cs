﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Inventario.App
{
    public partial class NuevaBodega : System.Web.UI.Page
    {
        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        protected void btnNuevaBodega_Click(object sender, EventArgs e)
        {


            if (txtNombre.Text != null && txtNombre.Text != "") {
                if (txtDireccion.Text != null && txtDireccion.Text != "")
                {
                    Bodega b = new Bodega(txtNombre.Text,txtDireccion.Text);
                    int newCodBodega = b.guardar();
                    Boolean success = newCodBodega > 0;
                    if (success)
                    {
                        b.iCodBodega = newCodBodega;
                        bMostrarMensajeExito = true;
                        sMensajeExito = "Nueva Bodega creada correctamente";
                        bMostrarMensajeExito = true;
                        txtDireccion.Text = "";
                        txtNombre.Text = "";
                    }
                    else {
                        bMostrarMensajeError = true;
                        sMensajeError = "No se ha podido guardar la bodega. ";
                        bMostrarMensajeError = true;
                    }
                    
                }
                else {
                    bMostrarMensajeError = true;
                    sMensajeError = "Debe de completar todos los campos con valores válidos";
                    bMostrarMensajeError = true;
                }
            }
            else
            {
                bMostrarMensajeError = true;
                sMensajeError = "Debe de completar todos los campos con valores válidos";
                bMostrarMensajeError = true;
            }



        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bMostrarMensajeError = bMostrarMensajeExito = false;
        }
    }
}