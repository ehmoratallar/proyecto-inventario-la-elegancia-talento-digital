﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using System.Collections;
using Inventario.App;

namespace Inventario.Authenticated
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        public ArrayList lstBodegas = Bodega.getBodegas(Bodega.getTotalBodegas(), 0);
        public Boolean CookieIsSet = false;
        public static HttpCookie Cookie = new HttpCookie("CodBodegaActual");
        public static int iCodBodegaActual;

        protected void Page_Load(object sender, EventArgs e)
        {
            object id = Membership.GetUser().ProviderUserKey;
            //InfoLabel.Text = id.ToString();


            if (Request.QueryString["setLocation"] != null)
            {
                Boolean isNum = false;
                
                isNum = int.TryParse(Request.QueryString["setLocation"], out iCodBodegaActual);

                if (isNum)
                {
                    //Create a Cookie with a suitable Key.
                    Cookie = new HttpCookie("CodBodegaActual");
                    //Set the Cookie value.
                    Cookie.Values["CodBodegaActual"] = Convert.ToString(iCodBodegaActual);
                    //Add the Cookie to Browser.
                    Response.Cookies.Add(Cookie);
                }

            }

            if (Request.QueryString["resetLocation"] != null)
            {
                //Fetch the Cookie using its Key.
                Cookie = Request.Cookies["CodBodegaActual"];
                //Set the Expiry date to past date.
                Cookie.Expires = DateTime.Now.AddDays(-1);
                //Update the Cookie in Browser.
                Response.Cookies.Add(Cookie);
                Response.Redirect("Main.aspx");

            }

            //Validamos si existe la cookie con la ubicación actual

            //Fetch the Cookie using its Key.
            Cookie = Request.Cookies["CodBodegaActual"];
            if (Cookie != null)
            {
                CookieIsSet = true;
                iCodBodegaActual = Convert.ToInt32(Cookie.Value.Split('=')[1]);
            }

            //Validamos si debemos mostrar mensajes al usuario
            if (Request.QueryString["sMensajeExito"] != null)
            {
                sMensajeExito = Request.QueryString["sMensajeExito"];
                bMostrarMensajeExito = true;
            }

            if (Request.QueryString["sMensajeError"] != null)
            {
                sMensajeError = Request.QueryString["sMensajeError"];
                bMostrarMensajeError = true;
            }
        }
    }
}