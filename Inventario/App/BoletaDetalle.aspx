﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="BoletaDetalle.aspx.cs" Inherits="Inventario.App.BoletaDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-ui.min.js"></script>\
    <script src="../Scripts/angular.js"></script>
    <link href="../Content/extra-css.css" rel="stylesheet" />
	<script src="../Scripts/simplePagination.js" ></script>
	<script src="../Scripts/jquery.validate.js" ></script>
	<script src="../Scripts/validarBoletaDetalle.js" ></script>
	<script src="../Scripts/app.js" ></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <div ng-app="inventario" ng-controller="productController as controller">
			
			<div class="row" ng-show="sMensajeError.length > 0">
				<div class="col-sm-12">
					<div class="alert alert-danger" role="alert">
						<strong>Error!</strong> {{sMensajeError}}
					</div>
				</div>
			</div>
			<div class="row" ng-show="sMensajeExito.length > 0">	
				<div class="col-sm-12">
					<div class="alert alert-success" role="alert">
						<strong>Éxito!</strong> {{sMensajeExito}}
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-sm-12">
					<div class="jumbotron">
						<div id="title">
							<i class="fa fa-pencil fa-5x"></i>
							<h1>Generar detalle de boleta</h1>
						</div>
					</div>
				</div>
			</div>
		
			
			<div class="row">
				<div class="col-sm-12">
					
						<pre ng-hide="true">{{products | json}}</pre>
					
				</div>
			</div>
			
			<!-- Formulario -->
			<div class="row" ng-show="showSelectPanel">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Seleccionar producto</h3>
						</div>
						<div class="panel-body">

							<form id="frmPrincipal" method="post">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Buscador de productos: </label>
											<div class="input-group">
												<input ng-model="productFilter" type="text" class="form-control" name="txtBusqueda" id="txtBusqueda" placeholder="Ingrese código o nombre de producto" />
												
												<span class="input-group-btn">
													<button ng-click="getProductos(productFilter)" class="btn btn-primary">Buscar</button>
												</span>
											</div>
										</div>
									</div>
								</div>

							</form>



								<!-- Crear la tabla -->

								<!-- Si no hay datos, mostrar mensaje -->
							<div ng-hide="products.length > 0">
								No hay productos disponibles
							</div>
							
							
							<div ng-show="products.length > 0">
								<!--//Definir tabla-->
								<div class="table-responsive">

									<table class="table table-hover">
										<!--//Crear headers-->
										<thead>
											<tr>
												<th>SKU</th>
												<th>Producto</th>
												<th>Precio</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										   
											<tr ng-repeat="producto in products | startFrom:currentPage*pageSize | limitTo:pageSize">
												<td>{{producto.sSKU}}</td>
												<td>{{producto.sNombre}}</td>
												<td>{{producto.dPrecio|currency:"Q"}}</td>
												<td><button ng-click="seleccionarProducto(producto)" class="btn btn-default">Seleccionar</button></td>
											</tr>
											

										</tbody>
										
									</table>
									
									<button class="btn btn-default" ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
										Página anterior
									</button>
									{{currentPage+1}}/{{numberOfPages()}}
									<button class="btn btn-default" ng-disabled="currentPage >= products.length/pageSize - 1" ng-click="currentPage=currentPage+1">
										Página siguiente
									</button>
									
								</div>
							</div>	
					
				
						</div>
					</div>
				</div>
			</div>
			<!-- Fin Formulario -->
			
			<!-- Formulario Insertar en detalle -->
			<div class="row" ng-hide="showSelectPanel">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left"><i class="fa fa-bar-chart-o fa-fw"></i>Agregar producto a boleta</h3>
							<button class="btn btn-danger pull-right" ng-click="showSelectPanel = true; BoletaNuevoDetalle = null">Seleccionar producto</button>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							<pre ng-hide="true">{{productoSeleccionado | json}}</pre>
							<p ng-hide="true"><strong>Codigo Bodega: </strong>  {{CodBoleta}}</p>
							
							<!-- Empieza Formulario -->
								
								<form id="frmBoleta" name="frmBoleta" novalidate class="redLabel" ng-submit="frmBoleta.$valid && agregarNuevoDetalle()">
									<div class="form-group">
										<label for="SKU">SKU:</label>
										<input ng-value="productoSeleccionado.sSKU" type="text" class="form-control" id="SKU" placeholder="SKU" readonly>
									</div>
									<div class="form-group">
										<label for="nombreProducto">Producto seleccionado:</label>
										<input ng-value="productoSeleccionado.sNombre" type="text" class="form-control" id="nombreProducto" placeholder="Producto" readonly>
									</div>
									<div class="form-group">
										<label for="precioProducto">Precio:</label>
										<input ng-value="productoSeleccionado.dPrecio|currency:'Q'" type="text" class="form-control" id="precioProducto" placeholder="Precio" readonly>
									</div>
									
									
									<!-- User input -->
									<div class="form-group">
										<label for="tipoMovimiento">Tipo de movimiento: </label>
										<select required ng-model="BoletaNuevoDetalle.TipoMovimiento" ng-change="BoletaNuevoDetalle.ProductoDaniado = 0" class="form-control" id="tipoMovimiento" name="tipoMovimiento" ng-model="BoletaTipoMovimiento" ng-options="x.sTipoMovimiento for x in BoletaTipoMovimientos">
										<option value="">Seleccionar</option>
										</select>
									</div>
                                    
                                    <div class="form-group" ng-show="BoletaNuevoDetalle.TipoMovimiento.iCodTipoMovimiento == 2">
                                        <label> Producto dañado:
                                            <input type="checkbox" ng-model="BoletaNuevoDetalle.ProductoDaniado" ng-true-value="1" ng-false-value="0">
                                        </label>
                                    </div>
									
									<div class="form-group">
										<label for="cantidad">Cantidad:</label>
										<input required ng-model="BoletaNuevoDetalle.Cantidad" type="number" min="0" step="1" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad">
									</div>
									
									<div class="form-group">
										<label for="justificacion">Justificación:</label>
										<input required ng-minlength="5" ng-model="BoletaNuevoDetalle.Justificacion" type="text" class="form-control" id="justificacion" name="justificacion" placeholder="Justificación">
									</div>
									
									<button type="submit" ng-disabled="frmBoleta.$invalid" class="btn btn-large btn-primary pull-right">Agregar a Detalle</button>
									
								</form>
							<!-- Termina formulario -->
							
							
						</div>
					</div>
				</div>
			</div>
			<!-- Fin Formulario Insertar en detalle -->
			
			
			<!-- Fila detalle de boleta -->
			<div class="row" ng-show="BoletaDetalleProductos.length > 0">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left"><i class="fa fa-bar-chart-o fa-fw"></i>Detalle de boleta</h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							
							<!-- Empieza Tabla -->
								
								<div class="table-responsive">

									<table class="table table-hover">
										<!--//Crear headers-->
										<thead>
											<tr>
												<th>SKU</th>
												<th>Producto</th>
												<th>Precio</th>
												<th>Movimiento</th>
												<th>Cantidad</th>
												<th>Justificación</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										   
											<tr ng-repeat="detalle in BoletaDetalleProductos ">
												<td>{{detalle.producto.sSKU}}</td>
												<td>{{detalle.producto.sNombre}}</td>
												<td>{{detalle.producto.dPrecio|currency:"Q"}}</td>
												<td><span class="label" ng-class="{'label-default' : detalle.iCodTipoMovimiento == 1, 'label-warning' : detalle.iCodTipoMovimiento == 2}">{{detalle.TipoMovimiento.sTipoMovimiento}}</span>
                                                    <span class="label label-danger" ng-show="detalle.ProductoDaniado > 0">Producto dañado</span>
												</td>
												<td><input ng-required="true" ng-model="detalle.iCantidad" class="form-control" type="number" step="1" min="0"></td>
												<td>{{detalle.sJustificacion}}</td>
												<td><button ng-click="remover(detalle)" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
											</tr>
											

										</tbody>
										
									</table>
									
									
									
								</div>
								
							<!-- Termina Tabla -->
							<button ng-click="guardarBoleta()" class="btn btn-large btn-primary pull-right">Guardar Boleta</button>
							<pre>{{BoletaDetalleProductos | json}}</pre>
							
						</div>
					</div>
				</div>
			</div>
			<!-- Fin Fila detalle de boleta -->
			
		</div>
    
</asp:Content>
