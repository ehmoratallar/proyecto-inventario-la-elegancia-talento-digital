﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NuevaBodega.aspx.cs" Inherits="Inventario.App.NuevaBodega" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <%-- Row --%>
    <div class="row">
        <%-- Mensaje Error --%>
        <%if (bMostrarMensajeError){ %>

        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <%= sMensajeError %>.
            </div>
        </div>
        <%} %>

        <%-- Mensaje Exito --%>
        <%if (bMostrarMensajeExito){ %>
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Éxito!</strong> <%= sMensajeExito %>.
            </div>
        </div>
        <%} %>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div id="title">
                    <i class="fa fa-plus fa-5x"></i>
                    <h1>Nueva Bodega</h1>
                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>

    <%-- Row --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Información Nueva Bodega</h3>
                </div>
                <div class="panel-body">

                    <form name="frmNuevaBodega" id="frmNuevaBodega" action="NuevaBodega.aspx" method="post" runat="server">
                        <div class="form-group">
                            <label>Nombre bodega: </label>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Direccion bodega: </label>
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                        </div>

                        <asp:Button ID="btnNuevaBodega" runat="server" Text="Crear Bodega" CssClass="btn btn-primary" OnClick="btnNuevaBodega_Click" />

                    </form>

                </div>
            </div>
        </div>
    </div>
    <%-- /Row --%>
</asp:Content>
