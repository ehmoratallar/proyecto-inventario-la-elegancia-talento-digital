﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventario.App
{
    public partial class NuevaBoleta : System.Web.UI.Page
    {

        public string sMensajeError = "";
        public string sMensajeExito = "";
        public Boolean bMostrarMensajeError, bMostrarMensajeExito = false;

        public static HttpCookie Cookie = new HttpCookie("CodBodegaActual");
        public static int iCodBodegaActual;
        public Boolean CookieIsSet = false;

        protected void btnNuevaBoleta_Click(object sender, EventArgs e)
        {
            Boleta boleta = new Boleta();


            
            boleta.iCodBodega = iCodBodegaActual;
            boleta.sDescripcion = txtDescripcion.Text;
            boleta.sCodEmpleado = Membership.GetUser().ProviderUserKey.ToString();
           



            int newCodBoleta = boleta.guardar();
            Boolean success = newCodBoleta > 0;
            if (success)
            {

                bMostrarMensajeExito = true;
                sMensajeExito = "Boleta creada correctamente. ";
                bMostrarMensajeExito = true;

                Response.Redirect("/App/BoletaDetalle.aspx?CodBoleta="+newCodBoleta);

            }
            else
            {
                bMostrarMensajeError = true;
                sMensajeError = "No se ha podido guardar el producto. ";
                bMostrarMensajeError = true;
            }

        }


        protected void btnCambiarUbicacion_Click(object sender, EventArgs e)
        {

            Response.Redirect("/App/Main.aspx?resetLocation=true");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //Validar si el usuario seleccionó una ubicación.
            Cookie = Request.Cookies["CodBodegaActual"];
            if (Cookie != null)
            {
                CookieIsSet = true;
                iCodBodegaActual = Convert.ToInt32(Cookie.Value.Split('=')[1]);

                //Inicializar el campo de fecha
                DateTimeOffset currentDate = DateTime.Now;
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.AppendFormat("{0:dd/MM/yyyy H:mm:ss}", currentDate);
                txtFecha.Text = sBuilder.ToString();
                //Mostrar nombre de usuario
                txtUsuario.Text = Membership.GetUser().UserName;
                //Mostrar Bodega actual
                txtBodega.Text = Bodega.get(iCodBodegaActual).sNombre;

            }
            else{
                //El usuario no ha seleccionado una ubicación, se redirige a main
                Response.Redirect("/App/Main.aspx?sMensajeError=Debe seleccionar una bodega antes de continuar");
            }



            //Validamos si debemos mostrar mensajes al usuario
            if (Request.QueryString["sMensajeExito"] != null)
            {
                sMensajeExito = Request.QueryString["sMensajeExito"];
                bMostrarMensajeExito = true;
            }

            if (Request.QueryString["sMensajeError"] != null)
            {
                sMensajeError = Request.QueryString["sMensajeError"];
                bMostrarMensajeError = true;
            }

        }
    }
}