﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Inventario.App
{
    public class BoletaDetalles
    {
        int CodBoleta = 0;
        int iCodProducto = 0;
        int iCodTipoMovimiento = 0;
        int iCodPrecio = 0;
        int iCantidad = 0;
        string sJustificacion = "";
        int iProductoDaniado = 0;

        public BoletaDetalles(int CodBoleta, int iCodProducto, int iCodTipoMovimiento, int iCodPrecio, int iCantidad, string sJustificacion, int iProductoDaniado) {
            this.CodBoleta = CodBoleta;
            this.iCodProducto = iCodProducto;
            this.iCodTipoMovimiento = iCodTipoMovimiento;
            this.iCodPrecio = iCodPrecio;
            this.iCantidad = iCantidad;
            this.sJustificacion = sJustificacion;
            this.iProductoDaniado = iProductoDaniado;
        }

        public void guardar() {
            int success = -1;


            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paBoletaDetalleNuevo", connection);

                object id = System.Web.Security.Membership.GetUser().ProviderUserKey;
                string sUserID = id.ToString();

     

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodBoleta", this.CodBoleta));
                cmd.Parameters.Add(new SqlParameter("@CodProducto", this.iCodProducto));
                cmd.Parameters.Add(new SqlParameter("@CodTipoMovimiento", this.iCodTipoMovimiento));
                cmd.Parameters.Add(new SqlParameter("@CodPrecio", this.iCodPrecio));
                cmd.Parameters.Add(new SqlParameter("@Cantidad", this.iCantidad));
                cmd.Parameters.Add(new SqlParameter("@Justificacion", this.sJustificacion)); 
                cmd.Parameters.Add(new SqlParameter("@ProductoDaniado", this.iProductoDaniado));

                connection.Open();
                success = Convert.ToInt32(cmd.ExecuteScalar());

                //this.iCodBoleta = success;

                connection.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                //return -1;
            }


            //return success;

        }

    }
}