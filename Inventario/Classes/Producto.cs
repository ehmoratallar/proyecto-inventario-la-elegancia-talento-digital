﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Inventario.App
{
    public class Producto
    {

        public int iCodProducto;
        public string sSKU;
        public string sNombre;
        public string sDescripcion;
        public string sFabricante;
        public DateTime FechaCreacion;
        public string sCreadoPor;
        public DateTime FechaActualizacion;
        public string sActualizadoPor;
        public int iCodPrecio;
        public decimal dPrecio;
        public decimal dCosto;

        public Producto() { }

        public Producto(int iCodProducto, string sSKU, string sNombre, string sDescripcion, string sFabricante, DateTime FechaCreacion, string sCreadoPor, DateTime FechaActualizacion, string sActualizadoPor,int iCodPrecio, decimal dPrecio, decimal dCosto) {
            this.iCodProducto = iCodProducto;
            this.sSKU = sSKU;
            this.sNombre = sNombre;
            this.sDescripcion = sDescripcion;
            this.sFabricante = sFabricante;
            this.FechaCreacion = FechaCreacion;
            this.sCreadoPor = sCreadoPor;
            this.FechaActualizacion = FechaActualizacion;
            this.sActualizadoPor = sActualizadoPor;
            this.iCodPrecio = iCodPrecio;
            this.dPrecio = dPrecio;
            this.dCosto = dCosto;
        }

        public int guardar()
        {
            int success = -1;


            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paProductoNuevo", connection);

                object id = System.Web.Security.Membership.GetUser().ProviderUserKey;
                string sUserID = id.ToString();


                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SKU", this.sSKU));
                cmd.Parameters.Add(new SqlParameter("@Nombre", this.sNombre));
                cmd.Parameters.Add(new SqlParameter("@Descripcion", this.sDescripcion));
                cmd.Parameters.Add(new SqlParameter("@Fabricante", this.sFabricante));
                cmd.Parameters.Add(new SqlParameter("@CreadoPor", sUserID));
                cmd.Parameters.Add(new SqlParameter("@Precio", this.dPrecio));
                cmd.Parameters.Add(new SqlParameter("@Costo", this.dCosto));


                connection.Open();
                success = Convert.ToInt32(cmd.ExecuteScalar());

                this.iCodProducto = success;

                connection.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }


            return success;
        }

        public Boolean editar()
        {
            Boolean success = false;
            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);

                object id = System.Web.Security.Membership.GetUser().ProviderUserKey;
                string sUserID = id.ToString();

                SqlCommand cmd = new SqlCommand("paProductoEditar", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodProducto", this.iCodProducto));
                cmd.Parameters.Add(new SqlParameter("@SKU", this.sSKU));
                cmd.Parameters.Add(new SqlParameter("@Nombre", this.sNombre));
                cmd.Parameters.Add(new SqlParameter("@Descripcion", this.sDescripcion));
                cmd.Parameters.Add(new SqlParameter("@Fabricante", this.sFabricante));
                cmd.Parameters.Add(new SqlParameter("@ActualizadoPor", sUserID));
                cmd.Parameters.Add(new SqlParameter("@Precio", this.dPrecio));
                cmd.Parameters.Add(new SqlParameter("@Costo", this.dCosto));

                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
                success = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return success;
        }


        public static Producto get(int iCodProducto)
        {
            Producto result = null;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT  [CodProducto] ,[SKU] ,[Nombre] ,[Descripcion] ,[Fabricante] ,[FechaCreacion] ,[CreadoPor] ,[FechaActualizacion] ,[ActualizadoPor] ,[Precio] ,[Costo] ,[TotalProductosDañados] ,[TotalProductosEnBuenEstado] FROM [Inventario].[dbo].[vProductoExistenciasTotales] WHERE [CodProducto] = @CodProducto;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@CodProducto", iCodProducto);

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                if (reader.Read())
                {


                    result = new Producto(
                        Convert.ToInt32(reader["CodProducto"]),
                        Convert.ToString(reader["SKU"]),
                        Convert.ToString(reader["Nombre"]),
                        Convert.ToString(reader["Descripcion"]),
                        Convert.ToString(reader["Fabricante"]),
                        Convert.ToDateTime(reader["FechaCreacion"]),
                        Convert.ToString(reader["CreadoPor"]),
                        Convert.ToDateTime(reader["FechaActualizacion"]),
                        Convert.ToString(reader["ActualizadoPor"]),
                        Convert.ToInt32(reader["CodPrecio"]),
                        Convert.ToDecimal(reader["Precio"]),
                        Convert.ToDecimal(reader["Costo"])
                        );

                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return result;
        }


        public static int getTotalProductos()
        {
            int result = 0;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT Count(1) as total FROM vProductoExistenciasTotales;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;



                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                if (reader.Read())
                {


                    result = Convert.ToInt32(reader["total"]);

                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }

            return result;
        }

        public static List<Producto> getProductos( int iResultadosPorPagina, int iPagina)
        {

            return getProductos("", iResultadosPorPagina, iPagina);
        }


        public static List<Producto> getProductos(string filtro, int iResultadosPorPagina, int iPagina)
        {
            List<Producto> lista = new List<Producto>();

            if (filtro == null || filtro == "undefined") {
                filtro = "";
            }

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT  [CodProducto] ,[SKU] ,[Nombre] ,[Descripcion] ,[Fabricante] ,[FechaCreacion] ,[CreadoPor] ,[FechaActualizacion] ,[ActualizadoPor] ,[CodPrecio],[Precio] ,[Costo] ,[TotalProductosDañados] ,[TotalProductosEnBuenEstado] FROM [Inventario].[dbo].[vProductoExistenciasTotales] WHERE ([SKU] LIKE '%' + @filtro + '%' OR [Nombre] LIKE '%' + @filtro + '%') ORDER BY [CodProducto] OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@filtro", filtro);
                cmd.Parameters.AddWithValue("@Limit", iResultadosPorPagina);
                cmd.Parameters.AddWithValue("@Offset", iPagina * iResultadosPorPagina);

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                while (reader.Read())
                {
                    // Console.WriteLine(String.Format("{0}", reader["id"]));
                    lista.Add(
                        new Producto(
                        Convert.ToInt32(reader["CodProducto"]),
                        Convert.ToString(reader["SKU"]),
                        Convert.ToString(reader["Nombre"]),
                        Convert.ToString(reader["Descripcion"]),
                        Convert.ToString(reader["Fabricante"]),
                        Convert.ToDateTime(reader["FechaCreacion"]),
                        Convert.ToString(reader["CreadoPor"]),
                        Convert.ToDateTime(reader["FechaActualizacion"]),
                        Convert.ToString(reader["ActualizadoPor"]),
                        Convert.ToInt32(reader["CodPrecio"]),
                        Convert.ToDecimal(reader["Precio"]),
                        Convert.ToDecimal(reader["Costo"])
                        )
                      );
                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return lista;
        }


        public static List<Producto> getProductosPorBodega(int iCodBodega, string sBusqueda, int iResultadosPorPagina, int iPagina)
        {
            List<Producto> lista = new List<Producto>();

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT [CodBodega] ,[CodProducto] ,[SKU] ,[Nombre] ,[Descripcion] ,[Fabricante] ,[FechaCreacion] ,[TotalProductosEnBuenEstado] ,[TotalProductosDañados]  FROM [Inventario].[dbo].[vProductoExistenciasPorBodega]  WHERE [CodBodega] = @CodBodega AND ([CodProducto] LIKE '%@sBusqueda%' OR [Nombre] LIKE '%@sBusqueda%')  ORDER BY [CodBodega] OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@CodBodega", iCodBodega); 
                cmd.Parameters.AddWithValue("@sBusqueda", sBusqueda);
                cmd.Parameters.AddWithValue("@Limit", iResultadosPorPagina);
                cmd.Parameters.AddWithValue("@Offset", iPagina * iResultadosPorPagina);


                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                while (reader.Read())
                {
                    // Console.WriteLine(String.Format("{0}", reader["id"]));
                    lista.Add(
                        new Producto(
                        Convert.ToInt32(reader["CodProducto"]),
                        Convert.ToString(reader["SKU"]),
                        Convert.ToString(reader["Nombre"]),
                        Convert.ToString(reader["Descripcion"]),
                        Convert.ToString(reader["Fabricante"]),
                        Convert.ToDateTime(reader["FechaCreacion"]),
                        Convert.ToString(reader["CreadoPor"]),
                        Convert.ToDateTime(reader["FechaActualizacion"]),
                        Convert.ToString(reader["ActualizadoPor"]),
                        Convert.ToInt32(reader["CodPrecio"]),
                        Convert.ToDecimal(reader["Precio"]),
                        Convert.ToDecimal(reader["Costo"])
                        )
                      );
                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return lista;
        }


        public Boolean eliminar()
        {
            Boolean success = false;
            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paProductoEliminar", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodProducto", this.iCodProducto));


                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
                success = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                throw new Exception(e.Message);
                return false;
            }

            return success;
        }

    }
}