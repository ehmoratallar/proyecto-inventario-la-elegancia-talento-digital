﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Inventario.App
{
    public class BoletaTipoMovimiento
    {
        public int iCodTipoMovimiento;
        public string sTipoMovimiento;

        public BoletaTipoMovimiento() {
            this.iCodTipoMovimiento = 0;
            this.sTipoMovimiento = ""; 
        }

        public BoletaTipoMovimiento(int iCodTipoMovimiento, string sTipoMovimiento)
        {
            this.iCodTipoMovimiento = iCodTipoMovimiento;
            this.sTipoMovimiento = sTipoMovimiento;
        }


        public static List<BoletaTipoMovimiento> get()
        {
            List<BoletaTipoMovimiento> lista = new List<BoletaTipoMovimiento>();



            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT [CodTipoMovimiento], [TipoMovimiento] FROM [dbo].[BoletaTipoMovimiento];";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;


                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                while (reader.Read())
                {
                    // Console.WriteLine(String.Format("{0}", reader["id"]));
                    lista.Add(
                        new BoletaTipoMovimiento(
                            Convert.ToInt32(reader["CodTipoMovimiento"]),
                            Convert.ToString(reader["TipoMovimiento"])   
                        )
                      );
                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return lista;
        }

    }
}