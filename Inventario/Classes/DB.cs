﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventario.App
{
    public class DB
    {

        public static string getConnectionString() {
            string sConexion = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Inventario.Properties.Settings.ConexionSQL"].ToString();
            return sConexion;
        }

    }
}