﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Inventario.App
{
    public class Boleta
    {
        public int iCodBoleta;
        public int iCodBodega;
        public string sDescripcion;
        public DateTime Fecha;
        public string sCodEmpleado;

        public Boleta() {
        }

        public Boleta(int iCodBoleta, int iCodBodega, string sDescripcion,DateTime Fecha, string sCodEmpleado )
        {
            this.iCodBoleta = iCodBoleta;
            this.iCodBodega = iCodBodega;
            this.sDescripcion = sDescripcion;
            this.Fecha = Fecha;
            this.sCodEmpleado = sCodEmpleado;
        }

        public Boleta(int iCodBodega, string sDescripcion, string sCodEmpleado) {
            this.iCodBodega = iCodBodega;
            this.sDescripcion = sDescripcion;
            this.sCodEmpleado = sCodEmpleado;
        }


        //Metodos
        public int guardar()
        {
            int success = -1;


            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paBoletaNueva", connection);

                object id = System.Web.Security.Membership.GetUser().ProviderUserKey;
                string sUserID = id.ToString();


                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodBodega", this.iCodBodega));
                cmd.Parameters.Add(new SqlParameter("@Descripcion", this.sDescripcion));
                cmd.Parameters.Add(new SqlParameter("@CodEmpleado", this.sCodEmpleado));
   


                connection.Open();
                success = Convert.ToInt32(cmd.ExecuteScalar());

                this.iCodBoleta = success;

                connection.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }


            return success;
        }


        public static Boleta get(int iCodBoleta)
        {
            Boleta result = null;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT [CodBoleta] ,[CodBodega] ,[Descripcion] ,[Fecha] ,[CodEmpleado] FROM [dbo].[Boleta] WHERE [CodBoleta] = @CodBoleta;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@CodBoleta", iCodBoleta);

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                if (reader.Read())
                {


                    result = new Boleta(
                        Convert.ToInt32(reader["CodBoleta"]),
                        Convert.ToInt32(reader["CodBodega"]),
                        Convert.ToString(reader["Descripcion"]),
                        Convert.ToDateTime(reader["Fecha"]),
                        Convert.ToString(reader["CodEmpleado"])
                        );

                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return result;
        }


    }
}