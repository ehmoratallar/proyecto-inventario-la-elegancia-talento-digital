﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Inventario.App
{
    public class Bodega
    {

        public string sNombre;
        public string sDireccion;
        public int iCodBodega;

        public Bodega()
        {
            this.sNombre = "";
            this.sDireccion = "";
        }

        public Bodega(string nombre, string direccion)
        {
            this.sNombre = nombre;
            this.sDireccion = direccion;
        }

        public Bodega(int codBodega, string nombre, string direccion)
        {
            this.iCodBodega = codBodega;
            this.sNombre = nombre;
            this.sDireccion = direccion;
        }

        public int guardar()
        {
            int success = -1;
            //if (this.iCodBodega != 0)
            //{

                try
                {
                    string sConnectionString = DB.getConnectionString();
                    SqlConnection connection = new SqlConnection(sConnectionString);
                    SqlCommand cmd = new SqlCommand("paBodegaNueva", connection);


                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Nombre", this.sNombre));
                    cmd.Parameters.Add(new SqlParameter("@Direccion", this.sDireccion));


                    connection.Open();
                    success = Convert.ToInt32(cmd.ExecuteScalar());

                    connection.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    return -1;
                }
            //}

            return success;
        }

        public Boolean editar()
        {
            Boolean success = false;
            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paBodegaEditar", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodBodega", this.iCodBodega));
                cmd.Parameters.Add(new SqlParameter("@Nombre", this.sNombre));
                cmd.Parameters.Add(new SqlParameter("@Direccion", this.sDireccion));

                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
                success = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return success;
        }


        public static int getTotalBodegas(){
            int result = 0;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT Count(1) as total FROM Bodega;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

               

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                if (reader.Read())
                {


                    result = Convert.ToInt32(reader["total"]);
                            
                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }

            return result;
        }

        public static ArrayList getBodegas(int iResultadosPorPagina, int iPagina)
        {
            ArrayList lista = new ArrayList();

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT CodBodega,Nombre,Direccion FROM Bodega ORDER BY CodBodega OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@Limit", iResultadosPorPagina);
                cmd.Parameters.AddWithValue("@Offset", iPagina * iResultadosPorPagina);

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                while (reader.Read())
                {
                    // Console.WriteLine(String.Format("{0}", reader["id"]));
                    lista.Add(
                        new Bodega(Convert.ToInt32(reader["CodBodega"])
                            , Convert.ToString(reader["Nombre"])
                            , Convert.ToString(reader["Direccion"])
                        )
                      );
                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return lista;
        }


        public static Bodega get(int iCodBodega)
        {
            Bodega result = null;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(DB.getConnectionString());
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "SELECT CodBodega, Nombre, Direccion FROM Bodega WHERE CodBodega = @CodBodega;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                cmd.Parameters.AddWithValue("@CodBodega", iCodBodega);

                sqlConnection.Open();

                reader = cmd.ExecuteReader();
                // Accediendo a los datos

                if (reader.Read())
                {


                    result = new Bodega(
                        Convert.ToInt32(reader["CodBodega"])
                        ,Convert.ToString(reader["Nombre"])
                        , Convert.ToString(reader["Direccion"])
                        );

                }

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return result;
        }


        public Boolean eliminar()
        {
            Boolean success = false;
            try
            {
                string sConnectionString = DB.getConnectionString();
                SqlConnection connection = new SqlConnection(sConnectionString);
                SqlCommand cmd = new SqlCommand("paBodegaEliminar", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CodBodega", this.iCodBodega));
                

                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
                success = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return success;
        }


    }
}