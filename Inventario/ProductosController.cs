﻿using Inventario.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventario
{
    public class ProductosController : ApiController
    {
        // GET: api/Productos
        public List<Producto> Get()
        {
            return Producto.getProductos(Producto.getTotalProductos(), 0);
        }

        // GET: api/Productos/5
        public Producto Get(int id)
        {
            return Producto.get(id);
        }

        [HttpGet]
        public List<Producto> BuscarProducto(string id)
        {
            return Producto.getProductos(id, Producto.getTotalProductos(), 0);
            //return "Buscar Producto " + id;
        }

        // POST: api/Productos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Productos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Productos/5
        public void Delete(int id)
        {
        }
    }
}
