﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace Inventario
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        public string sUserID = "";
        public string sUserName = "";



        protected void Page_Load(object sender, EventArgs e)
        {
            object id = Membership.GetUser().ProviderUserKey;
            sUserID = id.ToString();
            sUserName = Membership.GetUser().UserName;
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {

        }
    }
}