﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Inventario.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">

    <section class="container">
        <section class="login-form">
            <form method="post" action="/Login.aspx" id="Form1" name="frmLogin" runat="server">
                <div>
				    <img src="/Content/logo.png" alt="" />
				    <h4>Iniciar sesión</h4>
			    </div>
                <asp:Login ID="Login2" runat="server" FailureText="Fallo de inicio de sesión. Por favor intente nuevamente." LoginButtonText="Iniciar Sesión" PasswordLabelText="" RememberMeText="Mantener iniciada la sesión." TitleText="" PasswordRequiredErrorMessage="Se requiere una contraseña." UserNameRequiredErrorMessage="Se requiere el nombre de usuario." LoginButtonStyle-CssClass="btn btn-default" UserNameLabelText="" DestinationPageUrl="~/App/Main.aspx"></asp:Login>
            </form>
        </section>
    </section>

    <%-- Contenido de pagina 
    <div class="container">
        <div class="row">
            <div class="jumbotron">
                <h1>Iniciar sesión</h1>
            </div>
        </div>

        <div class="row">
            <div class="center-block col-sm-12">
                <form method="post" action="/Login.aspx" id="frmLogin" name="frmLogin" runat="server">
                    <div class="form-group">
                        <asp:Login ID="Login1" runat="server" FailureText="Fallo de inicio de sesión. Por favor intente nuevamente." LoginButtonText="Iniciar Sesión" PasswordLabelText="Contraseña:" RememberMeText="Mantener iniciada la sesión." TitleText="" PasswordRequiredErrorMessage="Se requiere una contraseña." UserNameRequiredErrorMessage="Se requiere el nombre de usuario." LoginButtonStyle-CssClass="btn btn-default" UserNameLabelText="Usuario:" DestinationPageUrl="~/App/Main.aspx"></asp:Login>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <%-- / Contenido de pagina --%>
</asp:Content>
