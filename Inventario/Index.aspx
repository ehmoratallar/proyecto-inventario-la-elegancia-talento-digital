﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Inventario.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPrincipal" runat="server">


    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <h1>Iniciar sesión</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-push-2">
            <form id="frmPrincipal" runat="server">
                <!-- LoginStatus Control -->
                <asp:LoginStatus ID="LoginStatus1" runat="server" />

                
                <!-- Muestra el usuario conectado -->
                <div class="alert alert-info">
                    <asp:LoginName ID="LoginName1" runat="server" />
                </div>
                
            </form>
        </div>
    </div>


</asp:Content>
