﻿using Inventario.App;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Inventario
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BoletaDetalleController : ApiController
    {

        [HttpGet]
        public List<BoletaTipoMovimiento> ObtenerBoletaTipoMovimiento() {
            return BoletaTipoMovimiento.get();
        }

        // GET: api/BoletaDetalle
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/BoletaDetalle/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/BoletaDetalle
        [HttpPost]
        public void Post(dynamic DynamicClass)
        {
            string Input = JsonConvert.SerializeObject(DynamicClass);
            JObject json = JObject.Parse(Input);

            //string snombreProducto = Convert.ToString(json["data"][0]["iCodBoleta"]);
            string snombreProducto = Convert.ToString(json["data"][0]["sNombre"]);

            //int iCodBoleta = DynamicClass.data[0].iCodBoleta;
            int iCodBoleta = Convert.ToInt32(json["data"][0]["iCodBoleta"]);

            int iCodProducto = 0;
            int iCodTipoMovimiento = 0;
            int iCodPrecio = 0;
            int iCantidad = 0;
            string sJustificacion = "";
            int iProductoDaniado = 0;

            BoletaDetalles detalle;

            for (int i = 0; i < json["data"].Count(); i++)
            {


                iCodProducto = Convert.ToInt32(json["data"][i]["iCodProducto"]);
                iCodTipoMovimiento = Convert.ToInt32(json["data"][i]["iCodTipoMovimiento"]);
                iCodPrecio = Convert.ToInt32(json["data"][i]["producto"]["iCodPrecio"]);
                iCantidad = Convert.ToInt32(json["data"][i]["iCantidad"]);
                sJustificacion = Convert.ToString(json["data"][i]["sJustificacion"]);
                iProductoDaniado = Convert.ToInt32(json["data"][i]["ProductoDaniado"]);

                detalle = new BoletaDetalles(
                    iCodBoleta,
                    iCodProducto,
                    iCodTipoMovimiento,
                    iCodPrecio,
                    iCantidad,
                    sJustificacion,
                    iProductoDaniado);

                detalle.guardar();

            }

            Console.WriteLine("Debug");

            //throw new Exception("Value" + data);
        }

        // PUT: api/BoletaDetalle/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/BoletaDetalle/5
        public void Delete(int id)
        {
        }
    }
}
