﻿using Inventario.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InventarioWebAPI.Controllers
{
    
    public class ProductsController : ApiController
    {

        // GET api/<controller>
        public List<Producto> Get()
        {
            return Producto.getProductos(Producto.getTotalProductos(), 0);
        }

        // GET api/<controller>/5
        public Producto Get(int id)
        {
            return Producto.get(id);
        }

        //// GET api/<controller>/<action>/query
        //public List<Producto> BuscarProducto(string id)
        //{
        //    return Producto.getProductos(id, Producto.getTotalProductos(), 0);
        //}

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

    }
}
